#!/bin/sh -e

REVISION=$1
VERSION=$2
TAR=../txw2_0.1+$VERSION.orig.tar.xz
DIR=txw2-0.1+$VERSION
TAG=$(echo "txw2-project-$VERSION" | sed -re's/~(alpha|beta)/-\1-/')

svn export -r $REVISION https://svn.java.net/svn/jaxb~version2/trunk/txw2 $DIR
XZ_OPT=--best tar -c -J -f $TAR \
    --exclude '*.jar' \
    --exclude '*.class' \
    --exclude '*.ipr' \
    --exclude '*.iml' \
    --exclude '.settings' \
    --exclude '.project' \
    --exclude '.classpath' \
    $DIR
rm -rf $DIR ../$TAG

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir && echo "moved $TAR to $origDir"
fi
